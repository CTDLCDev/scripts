#!/bin/bash
# Anthony DeCusati
# CTDLC
#
# Simple script to enable swap.  Assumes /dev/xvdf is the swap drive

SWAP_DEVICE="${SWAP_DEVICE:-/dev/xvdf}"

mkswap "$SWAP_DEVICE"
blkid "$SWAP_DEVICE" | sed -re 's/.*(UUID="[^"]+").*/\1  none   swap  defaults  0  0/' >> /etc/fstab
swapon -a
