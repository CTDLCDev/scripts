#!/bin/bash
# This script derived from https://docs.aws.amazon.com/systems-manager/latest/userguide/sysman-install-startup-linux.html

cd /tmp
wget https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/debian_amd64/amazon-ssm-agent.deb
sudo dpkg -i amazon-ssm-agent.deb
sudo systemctl enable amazon-ssm-agent
