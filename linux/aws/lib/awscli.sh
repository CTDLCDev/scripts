#!/bin/bash
# This script derived from https://docs.aws.amazon.com/cli/latest/userguide/awscli-install-linux.html

# Install JSON parser and Python pip
# JSON parser is used for processing api calls that return JSON
apt-get install -y jq python-pip

pip install awscli --upgrade
