#!/bin/bash
# Anthony DeCusati
# CTDLC
#
# This script pulls IAM credentials from instance metadata into the shell environment

# IAM Role
IAM="${IAM:-AmazonEC2RoleforSSM}"

# Get the region from metadata
export AWS_DEFAULT_REGION="$( 
  curl -s 'http://169.254.169.254/latest/meta-data/placement/availability-zone' \
  | sed -re 's/([0-9][0-9]*)[a-z]*$/\1/'
)"

# Get credentials from metadata
eval "$( 
  curl -s "http://169.254.169.254/latest/meta-data/iam/security-credentials/$IAM" \
  | grep 'AccessKeyId\|SecretAccessKey\|Token' \
  | sed -r \
    -e 's/AccessKeyId/AWS_ACCESS_KEY_ID/' \
    -e 's/SecretAccessKey/AWS_SECRET_ACCESS_KEY/' \
    -e 's/Token/AWS_SESSION_TOKEN/' \
    -e 's/\s*"([^"]+)"\s*:\s*("[^"]+").*/\1=\2/'
)"
