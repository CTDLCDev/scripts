#!/bin/bash
# Anthony DeCusati
# CTDLC
# 2/15/2018
#
# Script to configure rancher server instances
#
# Make sure there are no Windows EOL's (\r\n) or the nested scripts will fail
# Also, it's probably safest to keep all our scripts in bash (and not sh)
# Try to keep actual logic in library scripts. This file should just be calls to run()
# The objective with configuring this way is to have a single entry point from CloudFormation
# and to encourage code reuse.

USERDATA=1
BASE_URL="$( dirname $SCRIPT_URL )"
ENTRY="${ENTRY}"
ENVIRONMENT="${ENVIRONMENT}"
IAM="${IAM:-AmazonEC2RoleforSSM}"

###
# Utilities.
###

# Given a string input, attempt to build a valid URL
function buildURL () {
  # Return OK if a valid url is passed
  URL="$1"
  if isOK "$1"; then return; fi
  
  # First, see if it is an app
  URL="$BASE_URL/apps/$1"
  if isOK "$URL"; then return; fi
  
  # Check if it is a script
  URL="$BASE_URL/$1"
  if isOK "$URL"; then return; fi
  
  # Check the web
  URL="http://$1"
  if isOK "$URL"; then return; fi
  
  # Unable to determine valid URL
  unset URL
  return 1
}

# Function to check if the given URL is valid for this script
function isOK () {
  # check URL format
  echo "$1" | egrep -q '^https?://\w+\.\w+' || return 1
  
  # get the HTTP header
  RESULT="$(curl -s --head "$1")"
  
  # Check header for status, return no error if everything is okay
  ( echo "$RESULT" | head -n 1 | grep -q 200 ) \
    && ( echo "$RESULT" | grep -qi "Content-Type:.*text/plain" ) \
    && return 0
  
  # Not valid
  return 1
}

# If the URL is valid, evaluate it as a script in the current environment
function loadURL () {
  # Build the URL and load it into the current shell
  if buildURL "$1"; then
    eval "$( curl -s "$URL" )"
    return
  fi
  
  # unable to load URL, return error
  return 1
}

# Helper function to load SSM Parameters
function loadParam () {
  aws ssm get-parameters --name "$1" --with-decryption \
  | jq -r .Parameters[0].Value
}

# Load common entry
loadURL "common"
