#!/bin/bash
source "$( dirname $0 )/credential"

date=`date +%Y%m%d`
dateFormatted=`date -R`
contentType="application/octet-stream"

fileName="$1"
filePath="${2}/$( basename "$fileName" )"
relativePath="/${s3Bucket}${filePath}"
stringToSign="PUT\n\n${contentType}\n${dateFormatted}\n${relativePath}"
signature=`echo -en ${stringToSign} | openssl sha1 -hmac ${s3SecretKey} -binary | base64`

curl -X PUT -T "${fileName}" \
-H "Host: ${s3Bucket}.s3.amazonaws.com" \
-H "Date: ${dateFormatted}" \
-H "Content-Type: ${contentType}" \
-H "Authorization: AWS ${s3AccessKey}:${signature}" \
https://${s3Bucket}.s3.amazonaws.com${filePath}
