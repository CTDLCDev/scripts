@echo off
setlocal enableDelayedExpansion

set this=%~nx0
set credentials=%1
set file=%2
set filename=%~nx2
set bucket=%3
set prefix=%4

path %~dp0bin;%PATH%

:USAGE
if "%1"=="" (
  echo Usage: !this! CREDENTIAL_FILE UPLOAD_FILE BUCKET_NAME [PREFIX]
  echo   CREDENTIAL_FILE       File containing the access key information for aws
  echo   UPLOAD_FILE           File to upload
  echo   BUCKET_NAME           Name of S3 bucket for uploading
  echo   [PREFIX]              Path to pre-pend to uploaded file  ^(ex. /dir/a/b/c/^)
  echo.
  echo Set S3KEY and S3SECRET in CREDENTIAL_FILE. Ex:
  echo   S3KEY=AAA123
  echo   S3SECRET=SuperSecretyKey
  exit /b 1
)

IF NOT EXIST "!credentials!" (
  echo Credential file not found: "!credentials!"
  echo.
  CALL :USAGE
  exit /b 1
)

IF NOT EXIST "!file!" (
  echo Upload file not found: "!file!"
  echo.
  CALL :USAGE
  exit /b 1
)

IF "!bucket!"=="" (
  echo Must provide a bucket name
  echo.
  CALL :USAGE
  exit /b 1
)

REM Set variables from credential file
for /f "delims=" %%x in (!credentials!) do set %%x

REM Set date/time variables
for /f "tokens=1-4 delims=/ " %%a in ('date /t') do set dow=%%a&&set day=%%c&& set month=%%b&&set year=%%d
for /f "tokens=1-4 delims=:,. " %%h in ('echo %time%') do set hour=%%h&set min=%%i&set sec1=%%j&set sec2=%%k

REM Obtain three letter month value for DateValue HTTP Header
IF !month! == 01 set mname=Jan
IF !month! == 02 set mname=Feb
IF !month! == 03 set mname=Mar
IF !month! == 04 set mname=Apr
IF !month! == 05 set mname=May
IF !month! == 06 set mname=Jun
IF !month! == 07 set mname=Jul
IF !month! == 08 set mname=Aug
IF !month! == 09 set mname=Sep
IF !month! == 10 set mname=Oct
IF !month! == 11 set mname=Nov
IF !month! == 12 set mname=Dec

REM Set time as a 4 digit value if leading zero does not exist
for /f "tokens=1" %%u in ('echo %time%') do set t=%%u
IF "!hour:~1,1!"==":" set t=0!hour!

REM Obtain the ActiveBias value and convert to decimal 
for /f "tokens=3" %%a in ('reg query HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\TimeZoneInformation /v ActiveTimeBias ^| grep -i "ActiveTimeBias"') do set /a abias=%%a

REM Set the + or - sign variable to reflect the timezone offset
IF "!abias:~0,1!"=="-" (set si=+) ELSE (set si=-)
for /f "tokens=1 delims=-" %%t in ('echo !abias!') do set tzc=%%t

REM Calculate to obtain floating points (decimal values)
set /a tzd=100*!tzc!/60

REM Calculate the active bias to obtain the hour
set /a tze=!tzc!/60

REM Set the minutes based on the result of the floating point calculation
IF "!tzd!"=="0" (set en=00 && set si=)
IF "!tzd:~1!"=="00" (set en=00) ELSE IF "!tzd:~2!"=="00" (set en=00 && set tz=!tzd:~0,2!) 
IF "!tzd:~1!"=="50" (set en=30) ELSE IF "!tzd:~2!"=="50" (set en=30 && set tz=!tzd:~0,2!) 
IF "!tzd:~1!"=="75" (set en=45) ELSE IF "!tzd:~2!"=="75" (set en=45 && set tz=!tzd:~0,2!) 

REM Adding a 0 to the beginning of a single digit hour value
IF !tze! LSS 10 (set tz=0!tze!) 

REM Set the date/timestamp to meet required format
set dateValue=!dow!, !day! !mname! !year! !hour!:!min!:!sec1! !si!!tz!!en!

REM Preparing the HTTP header field
set resource=/!bucket!/!prefix!!filename!
set URL="https://!bucket!.s3.amazonaws.com/!prefix!!filename!"
set contentType=text/octet-stream

REM You MUST have two returns after set NL=^
set NL=^


set stringToSign=PUT!NL!!NL!!contentType!!NL!!dateValue!!NL!!resource!
<nul set /p ".=!stringToSign!" > put.tmp

for /f "tokens=*" %%a in ('type put.tmp ^| openssl sha1 -hmac !S3SECRET! -binary ^| base64') do set signature=%%a

REM Sending the data
curl -X PUT -T "%2" -H "Host: !bucket!.s3.amazonaws.com" -H "Date: !dateValue!" -H "Content-Type: !contentType!" -H "Authorization: AWS !S3KEY!:!signature!" !URL!

del put.tmp
